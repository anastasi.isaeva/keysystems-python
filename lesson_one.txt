1. Декоратор считающий количество вызовов функции.
2. Декоратор считающий время выполнения функции.
3. Проверить является ли входная строка палиндромом.
4. Посчитайте, сколько раз символ встречается в строке.
5. Найдите три ключа с самыми высокими значениями в словаре my_dict
6. Написать функцию, которая сливает два словаря в один.
7. Напишите функцию, которая принимает два списка и выводит все элементы первого, которых нет во втором.
8. Напишите функцию reverse_my_list(lst), которая принимает список и меняет местами его первый и последний элемент.
9. На вход функции my_func() подается список целых чисел. На выходе выполнения функци получить кортеж уникальных элементов списка в обратном порядке.
10. Напишите функцию tuple_sort(), которая сортирует кортеж, состоящий из целых чисел по возрастанию и возвращает его.
11. Написать функцию date, принимающую 3 аргумента — день, месяц и год. Вернуть True, если такая дата есть в нашем календаре, и False иначе.
12. Написать функцию date, принимающую 3 аргумента — день, месяц и год. Вернуть True, если такая дата есть в нашем календаре, и False иначе.
13. На входе функция string_to_set() получает строку. Преобразуйте их в множество.
14. На входе функция list_to_set() получает список. Преобразуйте их в множество.
15. Напишите функцию sum_range(a, z), которая суммирует все целые числа от значения a до величины z включительно
16. Напишите функцию sum_range(a, z), которая перемножает все целые числа от значения a до величины z включительно

17-18. Повторить уроки и выполнить с наложением своей (любой) предметной области вместо заданной в уроках  https://highload.today/python-obektno-orientirovannoe-programmirovanie-oop-1/
https://highload.today/python-obektno-orientirovannoe-programmirovanie-oop-praktika-2/
